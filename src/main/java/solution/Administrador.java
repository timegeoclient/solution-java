package solution;



import java.io.Serializable;


public class Administrador implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String email;
	private String nome;	
	private String senha;	
	private String usuario;
	private Empresa empresa;

	public Administrador() {
	}

	public Administrador(String email, String nome, String senha, String usuario, Empresa empresa) {
		this(1, email, nome, senha, usuario, empresa);
	}

	public Administrador(Integer id, String email, String nome, String senha, String usuario, Empresa empresa) {
		super();
		this.setId(id);
		this.setEmail(email);
		this.setNome(nome);
		this.setSenha(senha);
		this.setUsuario(usuario);
		this.setEmpresa(empresa);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}	
	
}