package solution;



import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class Colaborador implements Serializable {
	private static final long serialVersionUID = 1L;
	 
	private Integer id;
	private String chaveVerificacao;
	private String email;
	private String macAparelho;
	private String nome;
	private String numero;
	private String senha;
	private String soAparelho;
	private String usuario;
	private Boolean validacao;
	private String vesaoAparelho;	
	private Empresa empresa;	
	private List<Visitas> visitas;
	
	public Colaborador() {
	}
	
	public Colaborador(String chaveVerificacao, String email, String macAparelho, String nome, String numero,
			String senha, String soAparelho, String usuario, Boolean validacao, String vesaoAparelho, Empresa empresa,
			List<Visitas> visitas) {
		this(null, chaveVerificacao, email, macAparelho, nome, numero, senha, soAparelho, usuario, validacao, vesaoAparelho, empresa, visitas);
	}
	
	public Colaborador(String chaveVerificacao, String email, String macAparelho, String nome,
			String numero, String senha, String soAparelho, String usuario, Boolean validacao, String vesaoAparelho,
			Empresa empresa) {
		this(null, chaveVerificacao, email, macAparelho, nome, numero, senha, soAparelho, usuario, validacao, vesaoAparelho, empresa, new ArrayList<Visitas> ());
	}

	public Colaborador(Integer id, String chaveVerificacao, String email, String macAparelho, String nome,
			String numero, String senha, String soAparelho, String usuario, Boolean validacao, String vesaoAparelho,
			Empresa empresa, List<Visitas> visitas) {
		super();
		this.setId(id);
		this.setChaveVerificacao(chaveVerificacao);
		this.setEmail(email);
		this.setMacAparelho(macAparelho);
		this.setNome(nome);
		this.setNumero(numero);
		this.setSenha(senha);
		this.setSoAparelho(soAparelho);
		this.setUsuario(usuario);
		this.setValidacao(validacao);
		this.setVesaoAparelho(vesaoAparelho);
		this.setEmpresa(empresa);
		this.setVisitas(visitas);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChaveVerificacao() {
		return chaveVerificacao;
	}

	public void setChaveVerificacao(String chaveVerificacao) {
		this.chaveVerificacao = chaveVerificacao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMacAparelho() {
		return macAparelho;
	}

	public void setMacAparelho(String macAparelho) {
		this.macAparelho = macAparelho;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSoAparelho() {
		return soAparelho;
	}

	public void setSoAparelho(String soAparelho) {
		this.soAparelho = soAparelho;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Boolean getValidacao() {
		return validacao;
	}

	public void setValidacao(Boolean validacao) {
		this.validacao = validacao;
	}

	public String getVesaoAparelho() {
		return vesaoAparelho;
	}

	public void setVesaoAparelho(String vesaoAparelho) {
		this.vesaoAparelho = vesaoAparelho;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Visitas> getVisitas() {
		return visitas;
	}

	public void setVisitas(List<Visitas> visitas) {
		this.visitas = visitas;
	}

	public Visitas addVisita(Visitas visita) {
		getVisitas().add(visita);
		visita.setColaborador(this);
		return visita;
	}

	public Visitas removeVisita(Visitas visita) {
		getVisitas().remove(visita);
		visita.setColaborador(null);
		return visita;
	}
	
}