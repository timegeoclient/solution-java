package solution;



import java.io.Serializable;
import java.util.List;

public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;
	    	
	private Integer id;
	private String cnpj;
	private String nome;
	private List<Colaborador> colaboradores;
	private List<Localizacao> localizacoes;
	
	public Empresa() {
	}

	public Empresa(String cnpj, String nome, List<Colaborador> colaboradors, List<Localizacao> localizacoes) {
		this(null, cnpj, nome, colaboradors, localizacoes);
	}

	public Empresa(Integer id, String cnpj, String nome, List<Colaborador> colaboradores,
			List<Localizacao> localizacoes) {
		super();
		this.setId(id);
		this.setCnpj(cnpj);
		this.setNome(nome);
		this.setColaboradores(colaboradores);
		this.setLocalizacoes(localizacoes);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Colaborador> getColaboradorws() {
		return colaboradores;		
	}

	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}

	public List<Localizacao> getLocalizacoes() {
		return localizacoes;
	}

	public void setLocalizacoes(List<Localizacao> localizacoes) {
		this.localizacoes = localizacoes;
	}	
	
	
	public Localizacao addLocalizacao(Localizacao localizacao) {
		getLocalizacoes().add(localizacao);
		localizacao.setEmpresa(this);
		return localizacao;
	}

	public Localizacao removeLocalizacao(Localizacao localizacao) {
		getLocalizacoes().remove(localizacao);
		localizacao.setEmpresa(null);
		return localizacao;
	}
	
}